﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarPowerUp : MonoBehaviour {

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Player") {

			other.GetComponent<MyPlayerHealth> ().playerShooting.fireTimer = 5f;
			other.GetComponent<MyPlayerHealth> ().playerShooting.timeBetweenBullets = 0.10f;
			Destroy (transform.root.gameObject);
		}
	}
}
