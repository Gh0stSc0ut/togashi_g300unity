﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyPowerUpManager : MonoBehaviour {

	public MyPlayerHealth playerHealth;
	public Transform[] spawnPoints;
	public GameObject[] powerUp;
	public GameObject[] slots;
	public float[] spawnTime;

	public static MyPowerUpManager gc = null;

	void Awake () {
		if (gc == null) {
			gc = this;
		} else if (gc != this) {
			Destroy (gameObject);
		}
	}

	void Start () {
		//StartCoroutine ("SpawnLoop", Random.Range (0, 2));
		StartCoroutine ("SpawnLoop", 0);
		StartCoroutine ("SpawnLoop", 1);
		StartCoroutine ("SpawnLoop", 2);
	}

	IEnumerator SpawnLoop (int powerNum) {
		while (true) {
			if(playerHealth.currentHealth <= 0f) {
				yield break;
			}

			SpawnPowerUp (powerNum);
			//SpawnPowerUp (Random.Range (0, 2));
			yield return new WaitForSeconds (spawnTime[powerNum]);
		}

	}

	void SpawnPowerUp (int usePowerUp) {

		int spawnPointIndex = Random.Range (0, spawnPoints.Length);

		if (slots [spawnPointIndex] == null) {
			slots[spawnPointIndex] = Instantiate (powerUp [usePowerUp], spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation) as GameObject;
		}
	}
}
