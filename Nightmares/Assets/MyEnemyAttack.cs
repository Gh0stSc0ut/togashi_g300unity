﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEnemyAttack : MonoBehaviour {

	public float timeBetweenAttacks = 0.5f;
	public int attackDamage = 10;

	Animator anim;
	GameObject player;
	MyPlayerHealth playerHealth;
	MyEnemyHealth enemyHealth;
	bool playerInRange;
	float timer;
	public bool isBoss = false;

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <MyPlayerHealth> ();
		enemyHealth = GetComponent<MyEnemyHealth>();
		anim = GetComponent <Animator> ();
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject == player) {
			playerInRange = true;
		}
	}

	void OnTriggerExit (Collider other) {
		if (other.gameObject == player) {
			playerInRange = false;
		}
	}

	void Update () {
		timer += Time.deltaTime;

		if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0) {
			Attack ();
		}

		if (playerHealth.currentHealth <= 0) {
			anim.SetTrigger ("PlayerDead");
		}
	}

	void Attack () {
		timer = 0f;

		if (playerHealth.currentHealth > 0) {
			playerHealth.TakeDamage (attackDamage);
		}
		if (isBoss) {
			anim.SetTrigger ("Basic Attack");
		}
	}
}
