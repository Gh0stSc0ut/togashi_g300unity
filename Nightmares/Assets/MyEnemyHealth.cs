﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEnemyHealth : MonoBehaviour {

	public int startingHealth = 100;
	public int currentHealth;
	public float sinkSpeed = 2.5f;
	public int scoreValue = 10;
	public AudioClip deathClip;
	public bool isBoss = false;
	//public MyEnemyManager enemyManager;

	Animator anim;
	AudioSource enemyAudio;
	ParticleSystem hitParticles;
	CapsuleCollider capsuleCollider;
	MyEnemyManager enemyManager;

	bool isDead;
	bool isSinking;


	void Awake () {
		anim = GetComponent <Animator> ();
		enemyAudio = GetComponent <AudioSource> ();
		hitParticles = GetComponentInChildren <ParticleSystem> ();
		capsuleCollider = GetComponent <CapsuleCollider> ();
		enemyManager = GetComponent <MyEnemyManager> ();

		/*if (MyEnemyHealth.gc == null) {
			Instantiate (enemyManager);
		}*/

		currentHealth = startingHealth;
	}

	void Update () {
		if (isSinking) {
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		}
	}

	public void TakeDamage (int amount, Vector3 hitPoint) {

		hitParticles.transform.position = hitPoint;
		hitParticles.Play ();

		TakeDamage (amount);
	}

	public void TakeDamage (int amount) {
		if (isDead) 
			return;

		enemyAudio.Play ();

		currentHealth -= amount;

		if (currentHealth <= 0) {
			Death ();
		}
	}


	public void Death () {
		isDead = true;

		capsuleCollider.isTrigger = true;

		anim.SetTrigger ("Dead");

		enemyAudio.clip = deathClip;
		enemyAudio.Play ();

		if (isBoss) {
			MyEnemyManager.gc.spawnTime = MyEnemyManager.gc.resetSpawnTime;
		}
	}

	public void StartSinking () {
		GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true;
		isSinking = true;
		MyScoreManager.score += scoreValue;
		Destroy (gameObject, 2f);
	}
}
