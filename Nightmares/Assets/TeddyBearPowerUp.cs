﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyBearPowerUp : MonoBehaviour {

	public int healthAmount;

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Player") {
			other.GetComponent <MyPlayerHealth> ().AddHealth (healthAmount);
			Destroy (transform.root.gameObject);
		}
	}
}
