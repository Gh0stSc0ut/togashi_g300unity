﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

	Transform player;
	MyPlayerHealth playerHeath;
	MyEnemyHealth enemyHealth;
	UnityEngine.AI.NavMeshAgent nav;

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerHeath = player.GetComponent <MyPlayerHealth> ();
		enemyHealth = GetComponent <MyEnemyHealth> ();
		nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
	}

	void Update () {
		if (enemyHealth.currentHealth > 0 && playerHeath.currentHealth > 0) {
			nav.SetDestination (player.position);
		} else {
			nav.enabled = false;
		}
	}
}