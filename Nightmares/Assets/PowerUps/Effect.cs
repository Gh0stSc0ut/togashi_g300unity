﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {

	public float speed = 12f;
	public float timeBetweenBullets = 0.05f;
	public float heal = 20f;

	PlayerMove playerMove;
	MyPlayerShooting playerShooting;
	GameObject player;

	void Awake () {
		playerMove = GetComponent <PlayerMove> ();
		playerShooting = GetComponent <MyPlayerShooting> ();
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject == player) {
			Destroy(GameObject.FindGameObjectWithTag ("PowerUp"));
			print ("Destory");
		}
	}
}
