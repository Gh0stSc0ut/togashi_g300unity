﻿using UnityEngine;

public class MyPlayerShooting : MonoBehaviour {
	public int damagePerShot = 20;
	public float timeBetweenBullets = 0.15f;
	public float range = 100f;

	public static MyPlayerShooting gc = null;

	float timer;
	Ray shootRay = new Ray();
	RaycastHit shootHit;
	int shootableMask;
	ParticleSystem gunParticles;
	LineRenderer gunLine;
	AudioSource gunAudio;
	Light gunLight;
	float effectsDisplayTime = 0.2f;
	public float fireTimer = 5f;

	void Awake () {
		shootableMask = LayerMask.GetMask ("Shootable");
		gunParticles = GetComponent<ParticleSystem> ();
		gunLine = GetComponent <LineRenderer> ();
		gunAudio = GetComponent<AudioSource> ();
		gunLight = GetComponent<Light> ();

		if (gc == null) {
			gc = this;
		} else if (gc != this) {
			Destroy (gameObject);
		}
	}

	void Update () {
		timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0) {
			Shoot ();
		}

		if(timer >= timeBetweenBullets * effectsDisplayTime) {
			DisableEffects ();
		}
		fireTimer -= Time.deltaTime;
		if (fireTimer <= 0f) {
			timeBetweenBullets = 0.15f;
		}
	}


	public void DisableEffects () {
		gunLine.enabled = false;
		gunLight.enabled = false;
	}


	void Shoot () {
		timer = 0f;

		gunAudio.Play ();

		gunLight.enabled = true;

		gunParticles.Stop ();
		gunParticles.Play ();

		gunLine.enabled = true;
		gunLine.SetPosition (0, transform.position);

		shootRay.origin = transform.position;
		shootRay.direction = transform.forward;

		if(Physics.Raycast (shootRay, out shootHit, range, shootableMask)) {
			MyEnemyHealth enemyHealth = shootHit.collider.GetComponent <MyEnemyHealth> ();
			if(enemyHealth != null) {
				enemyHealth.TakeDamage (damagePerShot, shootHit.point);
			}
			gunLine.SetPosition (1, shootHit.point);
		} else {
			gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
		}
	}
}