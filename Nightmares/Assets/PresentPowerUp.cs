﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentPowerUp : MonoBehaviour {

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Player") {
			Shield.gc.gameObject.SetActive (true);
			Shield.gc.shieldTime = 10f;
			Destroy (transform.root.gameObject);
		}
	}
}
