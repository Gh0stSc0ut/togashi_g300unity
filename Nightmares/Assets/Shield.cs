﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

	public float shieldTime = 10f;
	public int shieldDamage = 500;

	public static Shield gc = null;

	public MeshRenderer mR;

	public float flickerSpeed;

	PlayerMove playerMovement;
	MyPlayerShooting playerShooting;

	void Start () {
		mR = GetComponent<MeshRenderer> ();
	}

	void Awake () {
		gameObject.SetActive (false);
		playerMovement = GetComponent <PlayerMove> ();
		playerShooting = GetComponentInChildren <MyPlayerShooting> ();

		if (gc == null) {
			gc = this;
		} else if (gc != this) {
			Destroy (gameObject);
		}

	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Enemy") {
			other.GetComponent <MyEnemyHealth> ().TakeDamage (shieldDamage);
			//Destroy(other.gameObject);

		}
	}

	void Update () {
		shieldTime -= Time.deltaTime;
		if (shieldTime <= 0f) {
			this.gameObject.SetActive (false);
		}
		Flicker ();
	}

	void Flicker () {

		if (shieldTime >= 4f) {
			mR.enabled = true;
		} if (shieldTime <= 4f) {

		mR.enabled = !mR.enabled;

		Invoke ("Flicker", flickerSpeed);
		}
	}
}
