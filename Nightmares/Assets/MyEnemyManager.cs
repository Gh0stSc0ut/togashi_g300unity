﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyEnemyManager : MonoBehaviour {
	public MyPlayerHealth playerHealth;
	public GameObject[] enemy;
	public float[] spawnTime;
	public float[] resetSpawnTime;
	public float[] halfSpawnTime;
	public Transform[] spawnPointsA;
	public Transform[] spawnPointsB;
	public Transform[] spawnPointsC;

	public static MyEnemyManager gc = null;

	void Awake () {
		if (gc == null) {
			gc = this;
		} else if (gc != this) {
			Destroy (gameObject);
		}
	}


	void Start () {
		//InvokeRepeating ("Spawn", spawnTime, spawnTime);
		StartCoroutine ("SpawnLoop", 0);
		StartCoroutine ("SpawnLoop", 1);
		StartCoroutine ("SpawnLoop", 2);
		StartCoroutine ("SpawnLoop", 3);
	}


	/*void Spawn (int enemyNum) {
		if(playerHealth.currentHealth <= 0f) {
			return;
		}

		SpawnEnemy (0);

	}*/


	IEnumerator SpawnLoop (int enemyNum) {
		while (true) {
			if(playerHealth.currentHealth <= 0f) {
				yield break;
			}

			SpawnEnemy (enemyNum);
			yield return new WaitForSeconds (spawnTime[enemyNum]);
		}

	}


	void SpawnEnemy (int useEnemy) {
		Transform[] useSpawnPoints = spawnPointsA;
		if (useEnemy == 1) {
			useSpawnPoints = spawnPointsB;
		} if (useEnemy == 2) {
			useSpawnPoints = spawnPointsC;
		} if (useEnemy == 3) {
			useSpawnPoints = spawnPointsC;
			spawnTime = halfSpawnTime;
		}

		int spawnPointIndex = Random.Range (0, useSpawnPoints.Length);

		Instantiate (enemy[useEnemy], useSpawnPoints[spawnPointIndex].position, useSpawnPoints[spawnPointIndex].rotation);
	}
	void OnUpdate () {
		print (spawnTime);
	}
}
